# Custom-Product-Tabs-Pro
The pro version of Custom Product Tabs

# Features

1. Global Tabs
- Create global saved tabs. This will apply your saved tab to all of your products.

2. Taxonomy Tabs
- Assign taxonomy terms to tabs. This will apply your saved tab to all of your products using the specified term(s).
- If you apply a taxonomy to a product, updating that product will add the saved tab.

3. WooCommerce Bulk Edit
- Assign saved tabs to products via WordPress'/WooCommerce's core bulk edit screen.

4. Search
- Make WordPress' standard site search also search Custom Product Tabs data
- Make WooCommerce's standard search widget also search Custom Product Tabs data

5. Premium Support
- Premium YIKES support with a support form accessible directly from your dashboard

6. Miscellaneous
- Remove the tab title from tab content site-wide without needing a filter
- From a saved tab, view which products are currently using that tab

