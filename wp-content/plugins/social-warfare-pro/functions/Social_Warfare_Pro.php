<?php

class Social_Warfare_Pro extends SWP_Addon {

	public function __construct( ) {
        parent::__construct();
        $this->name = 'Social Warfare - Pro';
        $this->key = 'pro';
        $this->product_id = 63157;
        $this->version = SWPP_VERSION;
		$this->load_classes();

        add_action( 'wp_loaded', array( $this, 'instantiate_addon') );

		$this->registration_update_notification();
		$this->initiate_plugin();
		$this->update_checker();

        add_filter( 'swp_registrations', array( $this, 'add_self' ) );
	}

	public function load_classes() {


            /**
             * The Social Network Classes
             *
             * This family of classes provides the framework and the model needed for creating
             * a unique object for each social network. It also provides for maximum extensibility
             * to allow addons even easier access than ever before to create and add more social
             * networks to the plugin.
             *
             */
            $social_networks = array(
                'Buffer',
                'Reddit',
                'Flipboard',
                'Email',
                'Hackernews',
                'Pocket',
                'Tumblr',
                'Whatsapp',
                'Yummly'
            );
            $this->load_files( '/functions/social-networks/', $social_networks);


			/**
			 * The Utilities Classes
			 *
			 */
			$utilities = array(
				'Meta_Box_Loader'
			);

			$this->load_files( '/functions/utilities/', $utilities );

    		require_once SWPP_PLUGIN_DIR . '/functions/admin/SWP_Pro_Options_Page.php';
	}


    /**
     * Instantiates the addon's functionality.
     *
     * @return void
     *
     */
	public function instantiate_addon() {
        if ( $this->is_registered()) :
            new SWP_Pro_Options_Page();
        else:
            // die("not registerd");
        endif;
	}


	/**
	 * Hook into the registration functions in core and add this plugin to the array
	 *
	 * @param  $array Array An array of registrations to be processed and handled
	 * @return $array Array The modified array of registrations to be processed
	 * @since  2.3.3 | 13 SEP 2017 | Created
	 * @access public
	 *
	 */
	public function social_warfare_pro_registration_key($array) {
	    $array['pro'] = array(
	        'plugin_name' => 'Social Warfare - Pro',
	        'key' => 'pro',
	        'product_id' => SWPP_ITEM_ID,
	        'version' => SWPP_VERSION,
	    );

	    return $array;
	}

	/**
	 * Defer the loading of functions.
	 * We don't want these functions to run until after core has loaded.
	 *
	 * @param  none
	 * @return none
	 *
	 */
	public function initiate_plugin() {

        require_once SWPP_PLUGIN_DIR . '/functions/meta-box/meta-box.php';
        require_once SWPP_PLUGIN_DIR . '/functions/utilities/utility.php';
        require_once SWPP_PLUGIN_DIR . '/functions/admin/post-options.php';
        require_once SWPP_PLUGIN_DIR . '/functions/frontend-output/SWP_Pro_Header_Output.php';

		new SWP_Pro_Header_Output();
		new SWP_Meta_Box_Loader();

		if ( is_admin() ) {
	        require_once SWPP_PLUGIN_DIR . '/functions/admin/SWP_Pro_Settings_Link.php';
			new SWP_Pro_Settings_link();
		}

	}


	/**
	 * The Plugin Update Checker
	 *
	 *
	 * @since 2.0.0 | Created | Update checker added when the plugin was split into core and pro.
	 * @since 2.3.3 | 13 SEP 2017 | Updated to use EDD's update checker built into core.
	 * @access public
	 *
	 */

	public function update_checker() {

	    // Make sure core is on a version that contains our dependancies
	    if (defined('SWP_VERSION') && version_compare(SWP_VERSION , '2.3.3') >= 0){

	        // Check if the plugin is registered
	        if( $this->is_registered() ) {

	            // retrieve our license key from the DB
	            $license_key = swp_get_license_key('pro');
	            $website_url = swp_get_site_url();

	            // setup the updater
	            $swed_updater = new SWP_Plugin_Updater( SWP_STORE_URL , SWPP_PLUGIN_FILE , array(
	            	'version'   => SWPP_VERSION,      // current version number
	            	'license'   => $license_key,      // license key
	            	'item_id'   => $this->product_id,      // id of this plugin
	            	'author'    => 'Warfare Plugins', // author of this plugin
	            	'url'       => $website_url,      // URL of this website
	                'beta'      => false              // set to true if you wish customers to receive beta updates
	                )
	            );
	        }
	    }
	}

	/**
	 * Registration Update Notification
	 *
	 *
	 * @since 2.3.0
	 * @access public
	 * @return void
	 *
	 */
	public  function registration_update_notification() {
	    $options = get_option( 'social_warfare_settings', array() );
	    if( !empty($options['premiumCode']) && empty( $options['pro_license_key'] ) ):
	        echo '<div class="notice-error notice is-dismissable"><p>' . __( '<b>Important:</b> We’ve just made some significant upgrades to your <i>Social Warfare - Pro</i> license. You will need to <a href="https://warfareplugins.com/my-account/">grab your license key</a> and re-register the plugin. Read <a href="https://warfareplugins.com/support/how-to-register-your-license-key/">the full details</a> to find out why this change was necessary.', 'social-warfare' ) . '</p></div>';
	    endif;
	}

    /**
     * Loads an array of related files.
     *
     * @param  string   $path  The relative path to the files home.
     * @param  array    $files The name of the files (classes), no vendor prefix.
     * @return none     The files are loaded into memory.
     *
     */
    private function load_files( $path, $files ) {
        foreach( $files as $file ) {

            //* Add our vendor prefix to the file name.
            $file = "SWP_" . $file;
            require_once SWPP_PLUGIN_DIR . $path . $file . '.php';
        }
    }

}
