<?php

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['description'] );      	// Remove the description tab

    return $tabs;

}


// Hiding Prices on all Free Sample Products
add_filter( 'woocommerce_get_price_html', function( $price, $product ) {
	if ( is_admin() ) return $price;
	// Hide for these category slugs / IDs
	$hide_for_categories = array( 'free-samples' );
	// Don't show price when its in one of the categories
	if ( has_term( $hide_for_categories, 'product_cat', $product->get_id() ) ) {
		return '';
	}
	return $price; // Return original price
}, 10, 2 );
add_filter( 'woocommerce_cart_item_price', '__return_false' );
add_filter( 'woocommerce_cart_item_subtotal', '__return_false' );



/**
 * Remove Categories from WooCommerce Product Category Widget
 *
 * @author   Ren Ventura
 */

//* Used when the widget is displayed as a dropdown
add_filter( 'woocommerce_product_categories_widget_dropdown_args', 'rv_exclude_wc_widget_categories' );
 //* Used when the widget is displayed as a list
add_filter( 'woocommerce_product_categories_widget_args', 'rv_exclude_wc_widget_categories' );
function rv_exclude_wc_widget_categories( $cat_args ) {
	$cat_args['exclude'] = array('127','99','100','101','166'); // Insert the product category IDs you wish to exclude
	return $cat_args;
}

//
// add_action( 'wp_print_scripts', 'my_deregister_javascript', 100 );
//
// function my_deregister_javascript() {
//   wp_deregister_script( 'mkhb-render' );
//   wp_deregister_script( 'mkhb-column' );
//   wp_deregister_style( 'mkhb-render' );
//   wp_deregister_style( 'mkhb-row' );
//   wp_deregister_style( 'mkhb-column' );
//    if ( !is_page('Our Roots') ) {
//     wp_deregister_script( 'jQuery-easing' );
//     wp_deregister_script( 'jQuery-timeline' );
//     wp_deregister_script( 'jQuery--vertical-timeline' );
//     wp_deregister_script( 'jQuery-mousew' );
//     wp_deregister_script( 'jQuery-customScroll' );
//     wp_deregister_script( 'rollover' );
//     wp_deregister_script( 'jquery-prettyPhoto' );
//     wp_deregister_script( 'ctimeline-admin-js' );
//     wp_deregister_script( 'isotope-pkgd' );
//     wp_deregister_script( 'classie' );
//     wp_deregister_script( 'progressButton' );
//     wp_deregister_script( 'selectorProgressButton' );
//     wp_deregister_script( 'modernizr.custom' );
//     wp_deregister_script( 'jQuery-ui' );
//     wp_deregister_script( 'jquery-ui-core' );
//     wp_deregister_script( 'jquery-ui-widget' );
//     wp_deregister_script( 'jquery-ui-sortable' );
//     wp_deregister_script( 'jquery-ui-draggable' );
//     wp_deregister_script( 'jQuery-customScroll' );
//     wp_deregister_style( 'ctimeline-admin-css' );
//     wp_deregister_style( 'ctimeline-thick-css' );
//     wp_deregister_style( 'normalize-css' );
//     wp_deregister_style( 'component-css' );
//     wp_deregister_style( 'timeline-css' );
//     wp_deregister_style( 'timeline-css' );
//     wp_deregister_style( 'customScroll-css' );
//     wp_deregister_style( 'prettyPhoto-css' );
//     wp_deregister_style( 'my_timeline_font_awesome' );
//     wp_deregister_script( 'my_timeline_added_script' );
//
//     wp_deregister_style( 'my_timeline_lineicons_css' );
//      }
// }

add_filter( 'gform_confirmation', 'ag_custom_confirmation', 10, 4 );
function ag_custom_confirmation( $confirmation, $form, $entry, $ajax ) {
	add_filter( 'fl_after_footer', 'ag_overlay');
	return '<script type="text/javascript">
	JQuery("body").addClass("message-sent");
	JQuery("#gform-notification a").click(function() {
		JQuery("#overlay,#gform-notification").fadeOut("normal", function() {
			JQuery(this).remove();
		});
	});
	</script><div id="overlay"></div><div id="gform-notification">' . $confirmation . '<a class="button" href="https://www.hearthwoodfloors.com/swiffer/">OK</a></div>';
}
add_action( 'woocommerce_before_order_notes', 'additional_hidden_checkout_field' );
function additional_hidden_checkout_field() {
    echo '<input type="hidden" name="promoCode" value="smplrqsttrspns">';
    echo '<input type="hidden" name="formType" value="sampleRequest">';
}
/**
 * @snippet       Save & Display Custom Field @ WooCommerce Order
 * @how-to        Get CustomizeWoo.com FREE
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 3.8
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */
 
add_action( 'woocommerce_checkout_update_order_meta', 'bbloomer_save_new_checkout_field' );
  
function bbloomer_save_new_checkout_field( $order_id ) { 
    if ( $_POST['formType'] ) update_post_meta( $order_id, '_formType', esc_attr( $_POST['formType'] ) );
	if ( $_POST['promoCode'] ) update_post_meta( $order_id, '_promoCode', esc_attr( $_POST['promoCode'] ) );
}
  
?>
