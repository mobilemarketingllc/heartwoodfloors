HEARTHWOOD README 5/15/18
--------------------------

functions.php
- snippet for hiding all prices from "Free Samples" product Category
- snippet for hiding certain product categories from the front end (must use the cat IDs)

--------------------------

WooCommerce Overrides:

/woocommerce/single-product/price.php: 26-28
- added "MK Love Holder", let's you see the heart icon on the product page
<div class="mk-love-holder">
	<?php echo Mk_Love_Post::send_love(); ?>
</div>

/woocommerce/single-product/short-description.php: 35-51
- moved the "Custom Product Description" from /woocommerce/single-product/tabs/descroption.php

/woocommerce/single-product/tabs/descroption.php
- removed the "Custom Product Description" section, moved to /woocommerce/single-product/short-description.php
